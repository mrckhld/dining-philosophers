//
//  main.swift
//  Dining Philosophers
//
//  Created by Michael Rockhold on 8/8/20.
//  Copyright © 2020 Michael Rockhold. All rights reserved.
//

import Foundation

let MAX_DINERS = 5

let workdone = DispatchSemaphore(value: 0)

class Table {

    struct DinerQueue {
        let index: Int
        let queue: DispatchQueue
        let neverlock: DispatchSemaphore
        let table: Table
        
        init(index: Int, table: Table) {
            self.index = index
            self.table = table
            queue = DispatchQueue(label: "diner\(index)", attributes: .concurrent)
            neverlock = DispatchSemaphore(value: 0)
        }
        
        func eat(interval: TimeInterval) {
            
            let myFork = table.rightForkFor(dinerIndex: index)
            let otherFork = table.leftForkFor(dinerIndex: index)
            
            table.dinerStartedEating(diner: self)
            
            // use "waiting for an impossible semaphore to time out" as surrogate for "eating"
            _ = neverlock.wait(timeout: DispatchTime.now()+interval)

            table.dinerStoppedEating(diner: self)
            
            otherFork.signal()
            myFork.signal()
            
            wait()
        }

        func wait() {
            
            queue.async {
                
                let rightFork = self.table.rightForkFor(dinerIndex: self.index)
                let leftFork = self.table.leftForkFor(dinerIndex: self.index)

                let gotRightFork = rightFork.wait(timeout: DispatchTime.now()+1) == .success
                let gotLeftFork = leftFork.wait(timeout: DispatchTime.now()+1) == .success
                
                if gotRightFork && gotLeftFork {
                    // You may eat!
                    self.eat(interval: TimeInterval.random(in: 3..<10))
                    
                } else {
                    
                    if gotRightFork {
                        rightFork.signal()
                    }
                    else if gotLeftFork {
                        leftFork.signal()
                    }
                    // go back to waiting
                    self.wait()
                }
            }
        }
    }

    
    let dinerCount: Int
    var dinerState: [Bool]
    let forks: [DispatchSemaphore]

    init(dinerCount: Int) {
        
        self.dinerCount = dinerCount
        
        dinerState = [Bool](repeating: false, count: dinerCount)
        
        forks = (0..<dinerCount).map { (fi) -> DispatchSemaphore in
            let f = DispatchSemaphore(value: 0)
            f.activate()
            f.signal()
            return f
        }
    }
        
    func serve() {
        let dinerQueues = (0..<dinerCount).map { (dinerIndex) -> DinerQueue in
            return DinerQueue(index: dinerIndex, table: self)
        }
        
        for diner in dinerQueues {
            diner.wait()
        }
    }
    
    func stateReport() -> String {
        return dinerState.map { (b) -> String in
            return b ? "1" : "0"
        }.joined()
    }
    
    func dinerStartedEating(diner: DinerQueue) {
        dinerState[diner.index] = true
        print("+\(diner.index) \(stateReport())")
    }
    
    func dinerStoppedEating(diner: DinerQueue) {
        dinerState[diner.index] = false
    }

    func rightForkFor(dinerIndex: Int) -> DispatchSemaphore {
        return forks[dinerIndex]
    }
    
    func leftForkFor(dinerIndex: Int) -> DispatchSemaphore {
        return forks[dinerIndex == self.dinerCount - 1 ? 0 : dinerIndex + 1]
    }
}


Table(dinerCount: MAX_DINERS).serve()

workdone.activate()
workdone.wait() // LOL
